/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Carlos Izquierdo Gonz�lez                                                  *
 *  December 2016										                       *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef GAME_BUG_HEADER
#define GAME_BUG_HEADER

#include "BoxSprite.hpp"
#include <string>

using cocos2d::Vec2;

namespace cocosgame
{

	class Bug : public BoxSprite
	{
	 
	private:

		Vec2 direction;						///< Direcci�n que sigue el bicho en su desplazamiento.

		int type;							///< Tipo de bicho.

		static const int minSpeed = 100;	///< Velocidad m�nima de movimiento.
		static const int maxSpeed = 200;	///< Velocidad m�xima de movimiento.
		float speed;						///< Velocidad utilizada.

		const float maxInactive = 1.5f;		///< Tiempo m�ximo de inactividad entre reapariciones.
		float currentTime;					///< Tiempo actual de inactividad.


	public:

		/// Constructor.
		Bug() : direction(0, 0)
		{
		}

	public:

		/// Crea un bicho del tipo recibido.
		static Bug * create(int type)
		{
			std::string sPath = "sprites/bug_" + std::to_string(type) + "_0.png";
			
			return BoxSprite::create< Bug >(sPath.c_str());
		}

	public:

		/// Funci�n llamada cada frame.
		void update(float timeDelta);

		/// Guarda el tipo de bicho y la posici�n inicial.
		void Start(int type, Vec2 location);

		/// Establece el movimiento del bicho otorg�ndole una posici�n objetivo.
		void SetMovement(Vec2 target);

		/// Devuelve verdadero o falso seg�n la posici�n X del bicho est� fuera de los par�metros recibidos.
		bool XOut(float minScreen, float maxScreen);

		/// Devuelve verdadero o falso seg�n la posici�n Y del bicho est� fuera de los par�metros recibidos.
		bool YOut(float minScreen, float maxScreen);

		/// Reinicia la posici�n del bicho y su nuevo desplazamiento.
		void Restart(Vec2 pos, Vec2 target);
				
		/// Devuelve el tipo de bicho.
		int GetType() 
		{
			return type;
		}

	};

}

#endif // GAME_BUG_HEADER
