#ifndef INFO_SCENE
#define INFO_SCENE

#include "cocos2d.h"
#include <string>

namespace cocosgame
{
	class InfoScene : public cocos2d::Layer
	{

	public:

		const std::string infoText = 
			"Crush the flying bugs to score points.\nBut be careful, some bugs may hurt you... \n This videogame was created by Carlos Izquierdo Gonzalez, January 2017.";

		/// Crea la escena.
		static cocos2d::Scene* createScene();

		/// Inicializa la escena.
		virtual bool init();		

		// implement the "static create()" method manually
		CREATE_FUNC(InfoScene);

	private:
		
		/// Cambia la escena por la escena anterior en cola.
		void CloseMenu(cocos2d::Ref* pSender);
	};
}


#endif // INFO_SCENE