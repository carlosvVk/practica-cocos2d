#ifndef PAUSE_SCENE
#define PAUSE_SCENE

#include "cocos2d.h"

namespace cocosgame
{
	class PauseScene : public cocos2d::Layer
	{

	public:

		/// Crea la escena.
		static cocos2d::Scene* createScene();

		/// Inicializa la escena.
		virtual bool init();

		// implement the "static create()" method manually
		CREATE_FUNC(PauseScene);

	private:

		/// Vuelve al men� principal.
		void GoMainMenu(cocos2d::Ref* pSender);

		/// Contin�a la partida.
		void ResumeGame(cocos2d::Ref* pSender);

		/// Reinicia el juego.
		void RestartGame(cocos2d::Ref* pSender);

		/// Abre la informaci�n.
		void OpenInfoMenu(cocos2d::Ref* pSender);
	};
}
#endif // PAUSE_SCENE