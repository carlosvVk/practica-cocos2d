#ifndef MAIN_MENU_SCENE
#define MAIN_MENU_SCENE

#include "cocos2d.h"

namespace cocosgame
{
	class MainMenuScene : public cocos2d::Layer
	{

	public:

		/// Crea la escena.
		static cocos2d::Scene* createScene();

		/// Inicializa la escena.
		virtual bool init();
		
		// implement the "static create()" method manually
		CREATE_FUNC(MainMenuScene);

	private:

		/// Cierra la aplicación.
		void CloseApp(cocos2d::Ref* pSender);

		/// Inicia el juego.
		void StartGame(cocos2d::Ref* pSender);

		/// Abre la información.
		void OpenInfo(cocos2d::Ref* pSender);
	};
}
#endif // MAIN_MENU_SCENE