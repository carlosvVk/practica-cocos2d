/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "MainMenuScene.hpp"
#include "GameScene.hpp"
#include "PauseScene.hpp"
#include "InfoScene.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;

namespace cocosgame
{
	/// Crea la escena.
	Scene* PauseScene::createScene()
	{
		auto scene = Scene::create();

		auto layer = PauseScene::create();

		scene->addChild(layer);

		return scene;
	}

	/// Inicializa la escena.
	bool PauseScene::init()
	{ 
		if (!Layer::init())
		{
			return false;
		}

		// Tama�o visible.
		auto visibleSize = Director::getInstance()->getVisibleSize();

		// Bot�n de continuar.
		auto resumeButton = MenuItemImage::create(
			"ui/ResumeButtonNormal.png",
			"ui/ResumeButtonPressed.png",
			CC_CALLBACK_1(PauseScene::ResumeGame, this));

		resumeButton->setPosition(Vec2( visibleSize.width / 2, visibleSize.height / 2 + resumeButton->getContentSize().height * 2.25f));

		// Bot�n de reinicio.
		auto restartButton = MenuItemImage::create(
			"ui/RestartButtonNormal.png",
			"ui/RestartButtonPressed.png",
			CC_CALLBACK_1(PauseScene::RestartGame, this));

		restartButton->setPosition(Vec2( visibleSize.width / 2, visibleSize.height / 2 + restartButton->getContentSize().height * 0.75f));

		// Bot�n de informaci�n.
		auto infoButton = MenuItemImage::create(
			"ui/InfoButtonNormal.png",
			"ui/InfoButtonPressed.png",
			CC_CALLBACK_1(PauseScene::OpenInfoMenu, this));

		infoButton->setPosition(Vec2( visibleSize.width / 2, visibleSize.height / 2 - infoButton->getContentSize().height * 0.75f));

		// Bot�n de salida.
		auto exitButton = MenuItemImage::create(
			"ui/ExitButtonNormal.png",
			"ui/ExitButtonPressed.png",
			CC_CALLBACK_1(PauseScene::GoMainMenu, this));

		exitButton->setPosition(Vec2( visibleSize.width / 2, visibleSize.height / 2 - exitButton->getContentSize().height * 2.25f));
		

		// Contenedor de botones.
		Vector<MenuItem*> MenuItems;

		MenuItems.pushBack(resumeButton);
		MenuItems.pushBack(restartButton);
		MenuItems.pushBack(infoButton);
		MenuItems.pushBack(exitButton);

		// Creamos un men�.
		auto menu = Menu::createWithArray(MenuItems);
		menu->setPosition(Vec2::ZERO);
		this->addChild(menu, 1);

		// Cartel de pausa.
		auto label = Label::createWithTTF("Pause", "fonts/Marker Felt.ttf", 36);

		label->setPosition(Vec2( visibleSize.width / 2,	 visibleSize.height - label->getContentSize().height * 2));
		this->addChild(label, 1);

		return true;
	}

	/// Vuelve al men� principal.
	void PauseScene::GoMainMenu(Ref* pSender)
	{
		auto myScene = MainMenuScene::createScene();
		Director::getInstance()->replaceScene(myScene);
	}

	/// Contin�a la partida.
	void PauseScene::ResumeGame(Ref* pSender)
	{
		Director::getInstance()->popScene();
	}

	/// Reinicia el juego.
	void PauseScene::RestartGame(Ref* pSender)
	{
		auto myScene = GameScene::createScene();
		// Transition Fade
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, myScene, Color3B::BLACK));
	}

	/// Abre la informaci�n.
	void PauseScene::OpenInfoMenu(Ref* pSender)
	{
		auto myScene = InfoScene::createScene();
		Director::getInstance()->pushScene(myScene);
	}
}