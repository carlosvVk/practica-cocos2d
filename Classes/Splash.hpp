/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef GAME_SPLASH_HEADER
#define GAME_SPLASH_HEADER

#include "BoxSprite.hpp"
#include <string>

using cocos2d::Vec2;

namespace cocosgame
{

	class Splash : public BoxSprite
	{
		
	private:

		const float speed = 1;	///< Velocidad a la que se hace transparente.


	public:

		// Constructor.
		Splash()
		{
			RandomRotation();
		}

	public:

		/// Crea un splash del tipo recibido.
		static Splash * create(int type)
		{
			std::string sPath = "sprites/splash_" + std::to_string(type) + ".png";
			
			return BoxSprite::create< Splash >(sPath.c_str());
		}

	public:

		/// Baja su opacidad y devuelve si ha llegado al m�ximo.
		bool IsTransparent()
		{
			setOpacity(getOpacity() - speed);
			return getOpacity() == 0;
		}

	private:

		/// Establece una rotaci�n aleatoria.
		void RandomRotation()
		{
			setRotation(rand() % 360);
		}
		
	};
}

#endif // GAME_SPLASH_HEADER
