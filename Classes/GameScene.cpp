/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "GameScene.hpp"
#include "PauseScene.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;

namespace cocosgame
{
	/// Crea la escena.
	Scene* GameScene::createScene()
	{
		auto scene = Scene::create();

		auto layer = GameScene::create();

		scene->addChild(layer);

		return scene;
	}

	/// Inicializamos la escena.
	bool GameScene::init()
	{
		if (!Layer::init())
		{
			return false;
		}
		
		InitUI();

		InitBugs();

		scheduleUpdate();

		return true;
	}

	/// Funci�n llamada cada frame.
	void GameScene::update(float delta) 
	{
		// Si la partida no acabada.
		if (!end)
		{
			// Si queda tiempo restante.
			if (currentTime < totalTime)
			{
				currentTime += delta;

				for (int i = 0; i < currentBugs; i++)
					bugs[i]->update(delta);

				if (spawn)
					BugStarter(delta);

				CheckBugs();

				// Va borrando los splashes que son transparentes.
				for (int i = splashes.size() - 1; i >= 0; i--)
				{
					if (splashes[i]->IsTransparent())
						splashes.erase(std::remove(splashes.begin(), splashes.end(), splashes[i]), splashes.end());						
				}
			}
			else
			{
				end = true;
			}				

		}		
	}
	
	/// Inicializa la interfaz de usuario.
	void GameScene::InitUI()
	{

		auto layer = Layer::create();
		this->addChild(layer);
		
		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->onTouchBegan = CC_CALLBACK_2(GameScene::touchBegan, this);
		layer->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, layer);

		// Tama�o visible.
		auto visibleSize = Director::getInstance()->getVisibleSize();

		// Imagen de fondo.
		auto background = Sprite::create("ui/background.png");

		background->setPosition(visibleSize.width / 2, visibleSize.height / 2);
		this->addChild(background, 0);

		// Bot�n de pausa
		auto pauseButton = MenuItemImage::create(
			"ui/PauseButtonNormal.png",
			"ui/PauseButtonPressed.png",
			CC_CALLBACK_1(GameScene::SetPause, this));

		pauseButton->setPosition(Vec2(visibleSize.width - pauseButton->getContentSize().width / 2, visibleSize.height - pauseButton->getContentSize().height / 2));
		pauseButton->setOpacity(220);

		// Creamos un men� con el bot�n.
		auto menu = Menu::create(pauseButton, NULL);

		menu->setPosition(Vec2::ZERO);
		this->addChild(menu, 1);

		// Cartel de puntuaci�n.
		scoreText = Label::createWithTTF("Score: 0", "fonts/Marker Felt.ttf", 36);

		scoreText->setPosition(Vec2(scoreText->getContentSize().width / 2, visibleSize.height - scoreText->getContentSize().height / 2));
		this->addChild(scoreText, 1);
	}

	/// Inicializa los bichos y los componentes principales.
	void GameScene::InitBugs()
	{
		end = false;
		score = 0;
		spawn = true;
		currentBugs = 0;
		currentTime = 0;

		SetTimeSpawn();
		CreateBugs();
	}
	
	/// Crea los bichos y los esconde.
	void GameScene::CreateBugs()
	{
		Bug * bug;

		for (int j = 0; j < maxBug.size(); j++)
		{
			for (int i = 0; i < maxBug[j]; i++)
			{
				bug = Bug::create(j);

				bug->Start(j, Vec2(-bug->getBoundingBox().size.width / 2, -bug->getBoundingBox().size.height / 2));

				this->addChild(bug, 2);
				bugs.push_back(bug);
			}
		}

		// Mezcla el contenedor para que no salgan en orden.
		std::random_shuffle(bugs.begin(), bugs.end());
	}
	
	/// Procesa el inicio de toque de pantalla.
	bool GameScene::touchBegan(cocos2d::Touch * touch, cocos2d::Event *)
	{		
		for (int i = 0; i < currentBugs; i++)
		{
			if (bugs[i]->getBoundingBox().containsPoint(touch->getLocation()))
			{
				// Crea un splash seg�n el bicho y lo introduce en la escena y el contenedor.

				Splash *newSplash = Splash::create(bugs[i]->GetType());
				newSplash->setPosition(bugs[i]->getPosition());

				this->addChild(newSplash, 0);
				splashes.push_back(newSplash);

				// Actualiza la puntuaci�n.

				score += bugScore[bugs[i]->GetType()];
				scoreText->setString("Score: " + std::to_string(score));

				// Reinicia el bicho.

				bugs[i]->Restart(GetSpawnPoint(bugs[i]), GetTarget());
			}
		}

		return true;
	}

	/// Va soltando los bichos cada cierto tiempo.
	void GameScene::BugStarter(float delta)
	{
		if (currentSpawnTime < spawnTime)
			currentSpawnTime += delta;
		else
			spawn = StartBug();
	}
	
	/// Inicializa el bicho y devuelve si se han inicializado todos.
	bool GameScene::StartBug()
	{
		SetTimeSpawn();
		bugs[currentBugs]->Restart(GetSpawnPoint(bugs[currentBugs]), GetTarget());
		currentBugs++;

		return currentBugs < bugs.size();
	}

	/// Llama a la funci�n "update" de cada bicho en movimiento.
	void GameScene::BugUpdate(float delta)
	{
		for (int i = 0; i < currentBugs; i++)
			bugs[i]->update(delta);
	}

	/// Comprueba si los bichos se han salido de pantalla para recolocarlos.
	void GameScene::CheckBugs()
	{
		int maxW = Director::getInstance()->getVisibleSize().width;
		int maxH = Director::getInstance()->getVisibleSize().height;
		
		for (int i = 0; i < currentBugs; i++)
		{
			if (bugs[i]->XOut(0, maxW) || bugs[i]->YOut(0, maxH))
				bugs[i]->Restart(GetSpawnPoint(bugs[i]), GetTarget());
		}		
	}
	
	/// Genera un punto de spawn para el bicho recibido.
	Vec2 GameScene::GetSpawnPoint(Bug * bug)
	{
		float coordX = 0;
		float coordY = 0;

		int maxW = Director::getInstance()->getVisibleSize().width;
		int maxH = Director::getInstance()->getVisibleSize().height;

		int random = 0;

		random = rand() % 2;

		switch (random)
		{
			// Le situamos a uno de los extremos laterales de la pantalla, con una altura aleatoria.
			case 0:
			{
				if (rand() % 2 == 0)
					coordX = -bug->getBoundingBox().size.width;
				else
					coordX = maxW + bug->getBoundingBox().size.width;

				coordY = rand() % maxH + (-bug->getBoundingBox().size.height);
				
				//coordX = (rand() % 2 == 0) ? -bug->getBoundingBox().size.width : maxW + bug->getBoundingBox().size.width;				

				break;
			}

			// Le situamos arriba o abajo, con una X aleatoria.
			case 1:
			{
				if (rand() % 2 == 0)
					coordY = -bug->getBoundingBox().size.height;
				else
					coordY = maxH + bug->getBoundingBox().size.height;

				coordX = rand() % maxW+ (-bug->getBoundingBox().size.width);

				break;
			}
		}

		return Vec2(coordX, coordY);
	}
	
	/// Devuelve una posici�n aleatoria a la que dirigirse dentro de la pantalla.
	Vec2 GameScene::GetTarget()
	{
		float coordX = 0;
		float coordY = 0;

		int maxW = Director::getInstance()->getVisibleSize().width;
		int maxH = Director::getInstance()->getVisibleSize().height;

		coordX = rand() % maxW;
		coordY = rand() % maxH;

		return Vec2(coordX, coordY);
	}

	/// Crea una instancia de la escena de pausa.
	void GameScene::SetPause(Ref* pSender)
	{
		auto myScene = PauseScene::createScene();
		Director::getInstance()->pushScene(myScene);
	}
	
}