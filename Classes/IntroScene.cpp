/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "IntroScene.hpp"
#include "MainMenuScene.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;

namespace cocosgame
{
	/// Crea la escena.
	Scene* IntroScene::createScene()
	{
		auto scene = Scene::create();

		auto layer = IntroScene::create();

		scene->addChild(layer);

		return scene;
	}

	/// Inicializa la escena.
	bool IntroScene::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		// Tama�o visible.
		auto visibleSize = Director::getInstance()->getVisibleSize();
		
		// Cartel de welcome.
		auto label = Label::createWithTTF("Welcome!", "fonts/Marker Felt.ttf", 36);

		label->setPosition(Vec2(visibleSize.width / 2,	visibleSize.height - label->getContentSize().height * 2));
		this->addChild(label, 1);

		// Inicializamos los valores.

		currentTime = 0;
		nextScene = false;

		this->scheduleUpdate();

		return true;
	}

	/// Funci�n llamada cada frame. Establece el cambio de escena.
	void IntroScene::update(float delta)
	{
		if (!nextScene)
		{
			currentTime += delta;

			if (currentTime >= maxTime)
			{
				auto myScene = MainMenuScene::createScene();
				nextScene = false;

				// Transition Fade
				Director::getInstance()->replaceScene(TransitionFade::create(0.5, myScene, Color3B::BLACK));
			}
		}

	}
}
