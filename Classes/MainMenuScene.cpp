/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "MainMenuScene.hpp"
#include "GameScene.hpp"
#include "InfoScene.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;

namespace cocosgame
{
	/// Crea la escena.
	Scene* MainMenuScene::createScene()
	{
		auto scene = Scene::create();

		auto layer = MainMenuScene::create();

		scene->addChild(layer);

		return scene;
	}

	/// Inicializa la escena.
	bool MainMenuScene::init()
	{ 
		if (!Layer::init())
		{
			return false;
		}


		// Tama�o visible.
		auto visibleSize = Director::getInstance()->getVisibleSize();

		// Bot�n de salida.
		auto exitButton = MenuItemImage::create(
			"ui/ExitButtonNormal.png",
			"ui/ExitButtonPressed.png",
			CC_CALLBACK_1(MainMenuScene::CloseApp, this));

		exitButton->setPosition(Vec2(visibleSize.width / 2,	visibleSize.height / 2 - exitButton->getContentSize().height * 2));

		// Bot�n de jugar.
		auto playButton = MenuItemImage::create(
			"ui/PlayButtonNormal.png",
			"v/PlayButtonPressed.png",
			CC_CALLBACK_1(MainMenuScene::StartGame, this));

		playButton->setPosition(Vec2(visibleSize.width / 2,	visibleSize.height / 2 + playButton->getContentSize().height * 2));

		// Bot�n de info.
		auto infoButton = MenuItemImage::create(
			"ui/InfoButtonNormal.png",
			"ui/InfoButtonPressed.png",
			CC_CALLBACK_1(MainMenuScene::OpenInfo, this));

		infoButton->setPosition(Vec2(visibleSize.width / 2,	visibleSize.height / 2));

		// Contenedor de botones.
		Vector<MenuItem*> MenuItems;

		MenuItems.pushBack(exitButton);
		MenuItems.pushBack(playButton);
		MenuItems.pushBack(infoButton);

		// Creamos un men�.
		auto menu = Menu::createWithArray(MenuItems);
		menu->setPosition(Vec2::ZERO);
		this->addChild(menu, 1);

		// Cartel de main menu.
		auto label = Label::createWithTTF("MainMenu!", "fonts/Marker Felt.ttf", 36);

		label->setPosition(Vec2(visibleSize.width / 2,	visibleSize.height - label->getContentSize().height * 2));
		this->addChild(label, 1);

		return true;
	}

	/// Cierra la aplicaci�n.
	void MainMenuScene::CloseApp(Ref* pSender)
	{
		Director::getInstance()->end();

		#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			exit(0);
		#endif
	}

	/// Inicia el juego.
	void MainMenuScene::StartGame(Ref* pSender)
	{
		auto myScene = GameScene::createScene();
		// Transition Fade
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, myScene, Color3B(0, 0, 0)));
	}

	/// Abre la informaci�n.
	void MainMenuScene::OpenInfo(Ref* pSender)
	{
		auto myScene = InfoScene::createScene();
		Director::getInstance()->pushScene(myScene);
	}
}