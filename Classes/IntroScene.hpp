#ifndef INTRO_SCENE
#define INTRO_SCENE

#include "cocos2d.h"

namespace cocosgame
{
	class IntroScene : public cocos2d::Layer
	{

	private:

		float currentTime;				///< Tiempo actual de escena.
		const float maxTime = 4.0f;		///< Tiempo m�ximo de escena.

		bool nextScene;					///< Almacena si ha de pasar a la siguiente escena.

	public:

		/// Crea la escena.
		static cocos2d::Scene* createScene();

		/// Inicializa la escena.
		virtual bool init();

		/// Funci�n llamada cada frame. Establece el cambio de escena.
		void update(float) override;

		// implement the "static create()" method manually
		CREATE_FUNC(IntroScene);
	};
}

#endif // INTRO_SCENE