/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "Bug.hpp"

namespace cocosgame
{

	/// Funci�n llamada cada frame.
	void Bug::update(float timeDelta)
	{
		if (currentTime > maxInactive)
			setPosition(getPosition() + direction * timeDelta);
		else
			currentTime += timeDelta;

		/*	ANIMACI�N SEG�N INTERNETE
		Vector<SpriteFrame*> animFrames(4);
		char str[100] = {0};
		for(int i = 1; i < 15; i++)
		{
		sprintf(str, "bug_%d_%d.png",type,i);
		auto frame = SpriteFrame::create(str,Rect(0,0,120,120)); //we assume that the sprites' dimentions are 40*40 rectangles.
		animFrames.pushBack(frame);
		}

		auto animation = Animation::createWithSpriteFrames(animFrames, 0.2f);
		auto animate = Animate::create(animation);
		sprite->runAction(animate);

		*/
	}

	/// Inicializa el bicho.
	void Bug::Start(int type, Vec2 location)
	{
		this->type = type;
		setPosition(location);
	}

	/// Establece el movimiento del bicho.
	void Bug::SetMovement(Vec2 target)
	{
		// Vector direcci�n.
		Vec2 newDir = target - getPosition();

		newDir = newDir.getNormalized();
		
		// Escalamos seg�n se necesite o no.
		if (newDir.x > 0)
			this->setScaleX(-1);
		else
			this->setScaleX(1);

		// Creamos velocidad random.
		speed = rand() % maxSpeed + minSpeed;

		direction = newDir * speed;
	}	

	/// Devuelve verdadero o falso seg�n la posici�n X del bicho est� fuera de los par�metros recibidos.
	bool Bug::XOut(float minScreen, float maxScreen)
	{
		return (minScreen > getPosition().x + (getBoundingBox().size.width) || maxScreen < (getPosition().x - getBoundingBox().size.width));
	}

	/// Devuelve verdadero o falso seg�n la posici�n Y del bicho est� fuera de los par�metros recibidos.
	bool Bug::YOut(float minScreen, float maxScreen)
	{
		return (minScreen > (getPosition().y + getBoundingBox().size.height) || maxScreen < (getPosition().y - getBoundingBox().size.height));
	}

	/// Reinicia la posici�n del bicho y su nuevo desplazamiento. Queda inm�vil al principio.
	void Bug::Restart(Vec2 pos, Vec2 target)
	{
		currentTime = 0;
		setPosition(pos);
		SetMovement(target);
	}
}