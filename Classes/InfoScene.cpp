/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                              *
*  Carlos Izquierdo Gonz�lez                                                   *
*  December 2016										                       *
*                                                                              *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "InfoScene.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;

namespace cocosgame
{
	/// Crea la escena.
	Scene* InfoScene::createScene()
	{
		auto scene = Scene::create();

		auto layer = InfoScene::create();

		scene->addChild(layer);

		return scene;
	}

	/// Inicializa la escena.
	bool InfoScene::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		// Tama�o visible.
		auto visibleSize = Director::getInstance()->getVisibleSize();

		// Bot�n de salida.
		auto exitButton = MenuItemImage::create(
			"ui/ExitButtonNormal.png",
			"ui/ExitButtonPressed.png",
			CC_CALLBACK_1(InfoScene::CloseMenu, this));

		exitButton->setPosition(Vec2(visibleSize.width - exitButton->getContentSize().width/2, visibleSize.height - exitButton->getContentSize().height/2));

		// Creamos un men� con el bot�n.
		auto menu = Menu::create(exitButton, NULL);

		menu->setPosition(Vec2::ZERO);
		this->addChild(menu, 1);

		// Cartel de Info.
		auto infoLabel = Label::createWithTTF(infoText, "fonts/Marker Felt.ttf", 24);
		infoLabel->setDimensions(visibleSize.width/2, visibleSize.height/2);

		infoLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height - infoLabel->getContentSize().height/1.5));
		this->addChild(infoLabel, 1);

		auto flySp = Sprite::create("sprites/bug_0_1.png");
		flySp->setPosition(visibleSize.width / 3, visibleSize.height / 3);

		this->addChild(flySp, 1);

		auto waspSp = Sprite::create("sprites/bug_1_1.png");
		waspSp->setPosition(visibleSize.width * 2 / 3, visibleSize.height / 3);

		this->addChild(waspSp, 1);

		return true;
	}

	/// Cambia la escena por la escena anterior en cola.
	void InfoScene::CloseMenu(Ref* pSender)
	{
		Director::getInstance()->popScene();
	}
}