/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Carlos Izquierdo Gonz�lez                                                  *
 *  December 2016										                       *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef GAME_BOXSPRITE_HEADER
#define GAME_BOXSPRITE_HEADER

#include <cocos2d.h>

namespace cocosgame
{

	class BoxSprite : public cocos2d::Sprite
	{

	protected:

		/// Cabecera de funci�n para crear los sprites.
		template< class DERIVED_CLASS >
		static DERIVED_CLASS * create(const char * textureName);
			
	protected:

		/// Constructor.
		BoxSprite()
		{
			this->setAnchorPoint(cocos2d::Vec2(0, 0));
		}

		/// Destructor.
		virtual ~BoxSprite()
		{

		}	
	};

	/// Funci�n para crear los sprites.
	template< class DERIVED_CLASS >
	DERIVED_CLASS * BoxSprite::create(const char * textureName)
	{
		std::unique_ptr< DERIVED_CLASS > sprite(new (std::nothrow) DERIVED_CLASS);

		if (sprite)
		{
			if (sprite->initWithFile(textureName))
			{
				sprite->autorelease();

				return sprite.release();
			}
		}

		return nullptr;
	}

}

#endif // GAME_BOXSPRITE_HEADER