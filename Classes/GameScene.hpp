#ifndef GAME_SCENE
#define GAME_SCENE

#include "cocos2d.h"
#include "Bug.hpp"
#include "Splash.hpp"

namespace cocosgame
{
	using cocos2d::Sprite;
	using cocos2d::Vec2;

	class GameScene : public cocos2d::Layer
	{

	private:
																
		//Layer * layer;									///< Capa donde situar los cosos que hay que cambiar.

		int score;										///< Puntuaci�n actual.
		cocos2d::Label * scoreText;						///< Cuadro de texto con la puntuaci�n.


		bool spawn;										///< Almacena si tiene que seguir spawneando bichos.			

		
		const float maxSpawn = 2.5f;					///< Tiempo m�ximo de spawn por bicho.
		const float minSpawn = 1;						///< Tiempo m�nimo de spawn por bicho.
		float spawnTime;								///< Tiempo spawn l�mite para el siguiente bicho.
		float currentSpawnTime;							///< Tiempo actual de spawn para el siguiente bicho.

		bool end;										///< Almacena si la partida se ha acabado.

		float currentTime;								///< Tiempo actual de partida.
		const float totalTime = 45;						///< Tiempo total que dura la partida.



		const std::vector<int> maxBug = { 10, 5 };		///< Contenedor de la cantidad m�xima de bichos de cada tipo.
		int currentBugs;								///< Cantidad actual de bichos activos.
		const std::vector<int> bugScore = { 2, -5 };	///< Contenedor de la cantidad de puntos que aporta cada tipo de bicho.
		
		std::vector< Splash* > splashes;				///< Contenedor de las manchas que dejan los bichos.
		std::vector< Bug* > bugs;						///< Contenedor de los bichos.

	public:

		/// Crea la escena.
		static cocos2d::Scene* createScene();			

		/// Inicializa la escena.
		virtual bool init();							

		/// Funci�n llamada cada frame.
		void update(float) override;					

		// implement the "static create()" method manually
		CREATE_FUNC(GameScene);

	private:
		
		/// Inicializa la interfaz de usuario.
		void InitUI();

		/// Inicializa los bichos y los componentes principales.
		void InitBugs();

		/// Crea los bichos y los esconde.
		void CreateBugs();

		/// Procesa el inicio de toque de pantalla.
		bool touchBegan(cocos2d::Touch * touch, cocos2d::Event *);
		
		/// Va soltando los bichos cada cierto tiempo.
		void BugStarter(float delta);

		/// Inicializa el bicho y devuelve si se han inicializado todos.
		bool StartBug();

		/// Reinicia el tiempo de spawn.
		void SetTimeSpawn()
		{
			spawnTime = rand() % (int)maxSpawn + minSpawn;
			currentSpawnTime = 0;
		}

		/// Llama a la funci�n "update" de cada bicho en movimiento.
		void BugUpdate(float delta);

		/// Comprueba si los bichos se han salido de pantalla para recolocarlos.
		void CheckBugs();

		/// Genera un punto de spawn para el bicho recibido.
		Vec2 GetSpawnPoint(Bug * bug);

		/// Devuelve una posici�n aleatoria a la que dirigirse dentro de la pantalla.
		Vec2 GetTarget();

		/// Crea una instancia de la escena de pausa.
		void SetPause(cocos2d::Ref* pSender);
		
	};
}

#endif // GAME_SCENE